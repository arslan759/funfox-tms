import React, { useState } from "react";
import "./Login.css";

const Login = ({ handleLogin }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const handleChange = (e) => {
    if (e.target.name === "email") {
      setEmail(e.target.value);
      setEmailError("");
    }

    if (e.target.name === "password") {
      setPassword(e.target.value);
      setPasswordError("");
    }
  };

  const fetchLogin = async () => {
    const response = await fetch("http://localhost:4000/users");

    const data = await response.json();

    const user = data.find((user) => user.email === email);

    if (!user) {
      setEmailError("Email is not registered");
      return;
    }

    if (user.password !== password) {
      setPasswordError("Password is incorrect");
      return;
    }

    return user;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (email == "" || password == "") {
      if (email === "") {
        setEmailError("Email is required");
      }

      if (password === "") {
        setPasswordError("Password is required");
      }

      return;
    }

    const user = await fetchLogin();

    if (user) {
      setEmail("");
      setPassword("");

      setEmailError("");
      setPasswordError("");

      const userGroup = JSON.stringify(user.group);

      localStorage.setItem("userGroup", userGroup);

      handleLogin();
    }
  };

  return (
    <div>
      <div className="AddTask">
        <h1
          style={{
            marginBottom: "20px",
          }}
        >
          Login
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="InputContainer">
            <label htmlFor="">Email</label>
            <input
              type="text"
              name="email"
              value={email}
              onChange={handleChange}
            />
            {emailError && <p className="InputError">{emailError}</p>}
          </div>

          <div className="InputContainer">
            <label htmlFor="">Password</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={handleChange}
            />
            {passwordError && <p className="InputError">{passwordError}</p>}
          </div>

          <div className="SubmitContainer">
            <button className="SubmitBtn" type="submit">
              Login
            </button>
          </div>
        </form>
        Credentials for Group A: <br />
        <br />
        Email: user@groupa.com <br />
        Password: 123456 <br />
        <br />
        Credentials for Group B: <br />
        <br />
        Email: user@groupb.com <br />
        Password: 123456
      </div>
    </div>
  );
};

export default Login;
