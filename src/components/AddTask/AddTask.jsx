import React, { useEffect, useState } from "react";
import "./AddTask.css";

const AddTask = ({ handleClose, groupName, setIsTaskAdded, isTaskAdded }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const [titleError, setTitleError] = useState("");
  const [descriptionError, setDescriptionError] = useState("");

  const handleChange = (e) => {
    if (e.target.name === "title") {
      setTitle(e.target.value);
      setTitleError("");
    }

    if (e.target.name === "description") {
      setDescription(e.target.value);
      setDescriptionError("");
    }
  };

  const handleAddTask = async () => {
    const response = await fetch("http://localhost:4000/groups");

    const data = await response.json();

    const group = data.find((group) => group.name === groupName);

    const newTask = {
      id:
        group.tasks.length === 0
          ? 1
          : group?.tasks[group.tasks.length - 1]?.id + 1,
      title: title,
      description: description,
      status: false,
    };

    group.tasks.push(newTask);

    const Update = await fetch(`http://localhost:4000/groups/${group.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(group),
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (title == "" || description == "") {
      if (title === "") {
        setTitleError("Title is required");
      }

      if (description === "") {
        setDescriptionError("Description is required");
      }

      return;
    }

    await handleAddTask();

    setIsTaskAdded(!isTaskAdded);

    setTitle("");
    setDescription("");

    setTitleError("");
    setDescriptionError("");

    window.location.reload();
  };

  useEffect(() => {}, [groupName]);

  return (
    <div>
      <div className="AddTask">
        <form onSubmit={handleSubmit}>
          <div className="InputContainer">
            <label htmlFor="">Title</label>
            <input
              type="text"
              name="title"
              value={title}
              onChange={handleChange}
            />
            {titleError && <p className="InputError">{titleError}</p>}
          </div>

          <div className="InputContainer">
            <label htmlFor="">Description</label>
            <textarea
              id=""
              cols="30"
              rows="10"
              name="description"
              value={description}
              onChange={handleChange}
            />
            {descriptionError && (
              <p className="InputError">{descriptionError}</p>
            )}
          </div>

          <div className="SubmitContainer">
            <button className="BackBtn" type="button" onClick={handleClose}>
              Back
            </button>
            <button className="SubmitBtn" type="submit">
              Add Task
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddTask;
