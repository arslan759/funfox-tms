import React, { useEffect, useState } from "react";
import Task from "../Task/Task";
import { handleFetchGroups } from "../../helpers/helpers";
import "./TaskList.css";

const TaskList = ({ groupName, isTaskAdded }) => {
  const [groups, setGroups] = useState([]);

  const fetchGroups = async () => {
    const groups = await handleFetchGroups();

    setGroups(groups);
  };

  const handleCompleteTask = async (id) => {
    const response = await fetch("http://localhost:4000/groups");

    const data = await response.json();

    const group = data.find((group) => group.name === groupName);

    const task = group.tasks.find((task) => task.id === id);

    task.status = !task.status;

    const Update = await fetch(`http://localhost:4000/groups/${group.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(group),
    });

    fetchGroups();
  };

  const handleDeleteTask = async (id) => {
    const response = await fetch("http://localhost:4000/groups");

    const data = await response.json();

    const group = data.find((group) => group.name === groupName);

    const filteredTasks = group.tasks.filter((task) => task.id !== id);

    group.tasks = filteredTasks;

    const Update = await fetch(`http://localhost:4000/groups/${group.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(group),
    });

    fetchGroups();
  };

  const filteredTasks = groups?.filter((group) => group?.name === groupName);

  useEffect(() => {
    fetchGroups();
    console.log("TaskList updated", isTaskAdded);
  }, [isTaskAdded]);

  useEffect(() => {}, [groupName]);

  return (
    <div className="TaskList">
      {filteredTasks[0]?.tasks?.map((task) => {
        const { id, title, description, status } = task;
        return (
          <Task
            key={id}
            title={title}
            id={id}
            status={status}
            description={description}
            handleDeleteTask={handleDeleteTask}
            handleCompleteTask={handleCompleteTask}
          />
        );
      })}
    </div>
  );
};

export default TaskList;
