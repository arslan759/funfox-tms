import React from "react";
import "./Task.css";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import DoneAllRoundedIcon from "@mui/icons-material/DoneAllRounded";
import DoneIcon from "@mui/icons-material/Done";

const Task = ({
  title,
  description,
  id,
  status,
  handleDeleteTask,
  handleCompleteTask,
}) => {
  return (
    <div className="Task">
      <div className="TaskInfo">
        <h3
          style={{
            textDecoration: status ? "line-through" : "none",
          }}
        >
          {title}
        </h3>
        <p>{description}</p>
      </div>

      <div className="TaskActions">
        {status ? (
          <DoneAllRoundedIcon
            onClick={() => handleCompleteTask(id)}
            sx={{
              color: "green",
              cursor: "pointer",
              transition: "all 0.3s ease-in-out",
              "&:hover": {
                color: "red",
                transform: "scale(1.2)",
              },
            }}
          />
        ) : (
          <DoneIcon
            onClick={() => handleCompleteTask(id)}
            sx={{
              color: "green",
              cursor: "pointer",
              transition: "all 0.3s ease-in-out",
              "&:hover": {
                color: "red",
                transform: "scale(1.2)",
              },
            }}
          />
        )}
        <DeleteForeverRoundedIcon
          onClick={() => handleDeleteTask(id)}
          sx={{
            color: "red",
            cursor: "pointer",
            transition: "all 0.3s ease-in-out",
            "&:hover": {
              color: "green",
              transform: "scale(1.2)",
            },
          }}
        />
      </div>
    </div>
  );
};

export default Task;
