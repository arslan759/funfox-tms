import { useEffect, useState } from "react";
import "./App.css";
import TaskList from "./components/TaskList/TaskList";
import AddTaskRoundedIcon from "@mui/icons-material/AddTaskRounded";
import AddTask from "./components/AddTask/AddTask";
import React from "react";
import Login from "./components/Login/Login";

function App() {
  const [isOpen, setisOpen] = useState(false);
  const [isLoggedIn, setisLoggedIn] = useState(false);
  const [groupName, setGroupName] = useState("");
  const [isTaskAdded, setIsTaskAdded] = useState(false);

  const handleAddTask = () => {
    setisOpen(true);
  };

  const handleClose = () => {
    setisOpen(false);
  };

  const handleLogin = () => {
    setisLoggedIn(true);

    localStorage.setItem("isLoggedIn", true);
  };

  const handleLogout = () => {
    setisLoggedIn(false);
    localStorage.removeItem("isLoggedIn");
    localStorage.removeItem("userGroup");
  };

  useEffect(() => {
    if (localStorage.getItem("isLoggedIn")) {
      setisLoggedIn(true);

      const userGroup = JSON.parse(localStorage.getItem("userGroup"));
      setGroupName(userGroup);
    }
  }, [isLoggedIn, groupName]);

  return (
    <div className="App">
      <div className="Main">
        <div className="TopBar">
          <h1 className="Header">Task Management System</h1>
          {isLoggedIn ? (
            <AddTaskRoundedIcon
              sx={{
                color: "white",
                cursor: "pointer",
              }}
              onClick={handleAddTask}
            />
          ) : null}
        </div>
        {isLoggedIn ? (
          <>
            <h2
              style={{
                color: "white",
                marginBottom: "20px",
              }}
            >
              {groupName}
            </h2>
            {isOpen ? (
              <AddTask
                handleClose={handleClose}
                groupName={groupName}
                setIsTaskAdded={setIsTaskAdded}
                isTaskAdded={isTaskAdded}
              />
            ) : (
              <TaskList groupName={groupName} isTaskAdded={isTaskAdded} />
            )}
          </>
        ) : (
          <Login handleLogin={handleLogin} />
        )}
        {isLoggedIn ? (
          <div className="LogoutContainer">
            <button className="LogoutBtn" onClick={handleLogout}>
              Logout
            </button>
          </div>
        ) : null}
      </div>
      <div></div>
    </div>
  );
}

export default App;
