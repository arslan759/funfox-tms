export const handleFetchGroups = async () => {
  const response = await fetch("http://localhost:4000/groups");

  const data = await response.json();

  return data;
};
